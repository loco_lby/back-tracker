import {
  Box,
  Button,
  Center,
  Divider,
  Heading,
  HStack,
  Tag,
  Text,
} from '@chakra-ui/react';

const SingleItem = ({ todo, openHandler, deleteHandler, isDeleteLoading }) => {
  return (
    <Box
      position='relative'
      borderWidth='1px'
      borderRadius='lg'
      w='100%'
      p='4'
      onClick={() => openHandler(todo)}
    >
      <HStack justifyContent={'space-between'}>
        <Heading size='md'>{todo.title}</Heading>

        <Button
          mt='4'
          size='sm'
          colorScheme='red'
          onClick={(event) => {
            event.stopPropagation();
            deleteHandler(todo.id);
          }}
          isDisabled={isDeleteLoading}
        >
          Delete
        </Button>
      </HStack>
      <Text noOfLines={[1, 2, 3]} color='gray.100'>
        {todo.description}
      </Text>
    </Box>
  );
};

export default SingleItem;
