import { useEffect, useRef } from 'react'

import mapboxgl from '!mapbox-gl' // eslint-disable-line import/no-webpack-loader-syntax

export default function Map({
  lng,
  lat,
  zoom,
  mapContainer,
  map,
  cursor,
  marker,
  markerMode,
}) {
  useEffect(() => {
    mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_API
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/locolby/cl2k8rvj0000115scndh90t4h',
      center: [lng || '70', lat || '40'],
      zoom: zoom || '1.5',
    })
    map.current.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
        showUserHeading: true,
      })
    )
  }, [])

  useEffect(() => {
    map.current.getCanvas().style.cursor = cursor
  }, [cursor])

  useEffect(() => {
    const mark = new mapboxgl.Marker()
    if (marker !== null) {
      mark.setLngLat([marker[0], marker[1]]).addTo(map.current)
    }
  }, [markerMode])

  return (
    <div>
      <div
        ref={mapContainer}
        style={{
          zIndex: '0',
          height: '100%',
          width: '100%',
          position: 'absolute',
        }}
      />
    </div>
  )
}
