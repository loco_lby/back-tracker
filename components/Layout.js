import React from 'react'
import Nav from './Nav'

export default function Layout({
  children,
  lng,
  lat,
  zoom,
  handleMarker,
  noteRef,
  countdown,
}) {
  return (
    <div
      style={{
        height: '100vh',
        width: '100%',
        overflow: 'hidden',
        position: 'relative',
      }}
    >
      <Nav
        lng={lng}
        lat={lat}
        zoom={zoom}
        handleMarker={handleMarker}
        noteRef={noteRef}
        countdown={countdown}
      />
      {children}
    </div>
  )
}
