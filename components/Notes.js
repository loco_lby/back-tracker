import {
  Alert,
  AlertIcon,
  Button,
  ButtonGroup,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Textarea,
  HStack,
  Box,
  VStack,
  ListItem,
} from '@chakra-ui/react'
import { useState, useEffect } from 'react'
import { supabase } from '../utils/supabaseClient'
import SingleItem from './SingleItem'
import { MdAddLocation } from 'react-icons/md'

const Notes = ({
  isOpen,
  onClose,
  noteRef,
  items,
  item,
  setItem,
  openHandler,
  deleteHandler,
  isDeleteLoading,
  countdown,
  setMarkerMode,
}) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [isLoading, setIsLoading] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  // useEffect(() => {
  //   const listener = (e) => {
  //     if (e.key === 'Escape') {
  //       onClose()
  //     }
  //   }
  //   document.addEventListener('keydown', listener)
  //   return () => {
  //     document.removeEventListener('keydown', listener)
  //   }
  // }, [])

  useEffect(() => {
    if (item) {
      setTitle(item.title)
      setDescription(item.description)
    }
  }, [item])

  const submitHandler = async (event) => {
    event.preventDefault()
    setErrorMessage('')

    setIsLoading(true)
    const user = supabase.auth.user()
    let supabaseError
    if (item) {
      const { error } = await supabase
        .from('todos')
        .update({ title, description, user_id: user.id })
        .eq('id', item.id)
      supabaseError = error
    } else {
      const { error } = await supabase
        .from('todos')
        .insert([{ title, description, user_id: user.id }])
      supabaseError = error
    }

    setIsLoading(false)
    if (supabaseError) {
      setErrorMessage(supabaseError.message)
    } else {
      closeHandler()
    }
  }

  const closeHandler = () => {
    setTitle('')
    setDescription('')

    setItem(null)
  }

  const handleMarker = () => {
    onClose()
    setMarkerMode(true)
  }

  return (
    <Modal
      scrollBehavior='inside'
      isCentered
      w='100%'
      onClose={onClose}
      initialFocusRef={noteRef}
      isOpen={isOpen}
    >
      <ModalOverlay />
      <ModalContent
        h='100vh'
        maxW='container.xl'
        bg='#091538'
        overflow={'scroll'}
        color='white'
        p={6}
      >
        <HStack spacing={12}>
          <Box minW='400px'>
            <form onSubmit={submitHandler}>
              <ModalHeader p={0}>{item ? 'Update Bag' : 'Add Bag'}</ModalHeader>
              <ModalCloseButton onClick={onClose} />
              <ModalBody p={0}>
                {errorMessage && (
                  <Alert status='error' borderRadius='lg' mb='6'>
                    <AlertIcon />
                    <Text textAlign='center'>{errorMessage}</Text>
                  </Alert>
                )}
                <FormControl isRequired={true}>
                  <FormLabel>Title</FormLabel>
                  <Input
                    ref={noteRef}
                    placeholder='Add your title here'
                    onChange={(event) => setTitle(event.target.value)}
                    value={title}
                  />
                </FormControl>

                <FormControl mt={4} isRequired={true}>
                  <FormLabel>Description</FormLabel>
                  <Textarea
                    placeholder='Add your description here'
                    onChange={(event) => setDescription(event.target.value)}
                    value={description}
                  />
                </FormControl>
              </ModalBody>

              <ModalFooter>
                <ButtonGroup spacing='3'>
                  <Button
                    onClick={onClose}
                    colorScheme='red'
                    type='reset'
                    isDisabled={isLoading}
                  >
                    Cancel
                  </Button>
                  <Button
                    colorScheme='blue'
                    type='submit'
                    isLoading={isLoading}
                  >
                    {item ? 'Update' : 'Save'}
                  </Button>
                  <Button
                    rightIcon={<MdAddLocation />}
                    colorScheme='green'
                    disabled={!title || !description || countdown > 0}
                    onClick={handleMarker}
                  >
                    {countdown > 0
                      ? `Wait ${(countdown / 3600000).toFixed(0)} hours`
                      : 'Add To Map'}
                  </Button>
                </ButtonGroup>
              </ModalFooter>
            </form>
          </Box>
          <VStack h='100%' pt={16} w='100%' spacing={6}>
            {items.map((item) => (
              <SingleItem
                todo={item}
                key={item.id}
                openHandler={openHandler}
                deleteHandler={deleteHandler}
                isDeleteLoading={isDeleteLoading}
              />
            ))}
          </VStack>
        </HStack>
      </ModalContent>
    </Modal>
  )
}

export default Notes
