import {
  Box,
  Button,
  Heading,
  Input,
  VStack,
  chakra,
  Stack,
  FormControl,
  FormLabel,
  Alert,
  AlertIcon,
  Text,
} from '@chakra-ui/react';
import { useState } from 'react';
import { supabase } from '../utils/supabaseClient';

export default function Auth() {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [error, setError] = useState(null);

  const submitHandler = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    setError(null);
    try {
      const { error } = await supabase.auth.signIn({
        email,
      });
      if (error) {
        setError(error.message);
      } else {
        setIsSubmitted(true);
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const changeHandler = (event) => {
    setEmail(event.target.value);
  };

  return (
    <Box>
      <Heading color='white' textAlign='center' mb='8'>
        Welcome to Backtracker
      </Heading>
      {error && (
        <Alert status='error' mb='6'>
          <AlertIcon />
          <Text textAlign='center'>{error}</Text>
        </Alert>
      )}
      <Box
        py='8'
        px={{ base: '4', md: '10' }}
        shadow='base'
        rounded={{ sm: 'lg' }}
        bg='white'
      >
        {isSubmitted ? (
          <Heading size='md' textAlign='center' color='gray.600'>
            Please check {email} for login link
          </Heading>
        ) : (
          <chakra.form onSubmit={submitHandler}>
            <Stack spacing='6'>
              <FormControl id='email'>
                <FormLabel>Email address</FormLabel>
                <Input
                  name='email'
                  type='email'
                  autoComplete='email'
                  required
                  value={email}
                  onChange={changeHandler}
                />
              </FormControl>
              <Button
                type='submit'
                colorScheme='blue'
                size='lg'
                fontSize='md'
                isLoading={isLoading}
              >
                Sign in
              </Button>
            </Stack>
          </chakra.form>
        )}
      </Box>
    </Box>
  );
}
