import { useState, useEffect, useRef } from 'react'

import {
  Box,
  HStack,
  Text,
  VStack,
  Select,
  Heading,
  IconButton,
  Tooltip,
} from '@chakra-ui/react'
import { MdOutlineExitToApp } from 'react-icons/md'
import { FaMapMarkerAlt } from 'react-icons/fa'
import { useRouter } from 'next/router'
import { supabase } from '../utils/supabaseClient'

export default function Nav({ handleMarker, noteRef, countdown }) {
  const router = useRouter()
  const [isLogoutLoading, setIsLogoutLoading] = useState(false)
  const user = supabase.auth.user()

  const logoutHandler = async () => {
    try {
      setIsLogoutLoading(true)
      await supabase.auth.signOut()
      router.push('/login')
    } catch (error) {
      router.push('/login')
    } finally {
      setIsLogoutLoading(false)
    }
  }

  return (
    <Box pos='relative' zIndex={2} w='100%'>
      <Box pos='absolute' left='2%' top='20px'>
        <Heading
          as='h1'
          fontSize={{ base: 'xl', md: '2xl' }}
          py={3}
          color='white'
          fontWeight={600}
        >
          Potential Moves
        </Heading>
      </Box>
      <HStack
        pos='absolute'
        left='50%'
        transform={'translateX(-50%)'}
        top='20px'
        maxW='container.xl'
      >
        <HStack
          p='15px'
          h='75px'
          borderRadius={'10px'}
          bg={{ base: 'rgba(255, 255, 255, 0.05)' }}
          backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
          spacing={4}
        >
          <Select
            w='300px'
            h='50px'
            borderRadius={'10px'}
            bg={{ base: 'rgba(255, 255, 255, 0.05)' }}
            backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
            color='white'
            fontWeight={600}
            fontSize={'xl'}
            _hover={{ bg: 'rgba(255, 255, 255, 0.05)' }}
            _focus={{ bg: 'rgba(255, 255, 255, 0.05)' }}
          >
            <option value='1'>Personal Moves</option>
            <option value='2'>Public Moves</option>
          </Select>
          <Tooltip label={'Add Marker'} bg={'white'} color={'black'}>
            <IconButton
              aria-label='Add Marker'
              icon={<FaMapMarkerAlt />}
              bg={{ base: 'rgba(255, 255, 255, 0.05)' }}
              backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
              color='white'
              fontSize='2xl'
              h='50px'
              w='50px'
              border='1px solid white'
              onClick={handleMarker}
              ref={noteRef}
              _hover={{ bg: 'rgba(255, 255, 255, 0.05)', borderColor: 'white' }}
            />
          </Tooltip>
          <Tooltip label={'Logout'} bg={'white'} color={'black'}>
            <IconButton
              aria-label='Logout'
              icon={<MdOutlineExitToApp />}
              bg={{ base: 'rgba(255, 255, 255, 0.05)' }}
              backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
              color='white'
              fontSize='3xl'
              h='50px'
              w='50px'
              border='1px solid white'
              _hover={{ bg: 'rgba(255, 255, 255, 0.05)', borderColor: 'white' }}
              onClick={logoutHandler}
              isLoading={isLogoutLoading}
            />
          </Tooltip>
        </HStack>
      </HStack>
      <HStack pos='absolute' right='20px' top='20px' maxW='container.xl'>
        <Box
          pos='relative'
          w='200px'
          h='75px'
          borderRadius={'10px'}
          bg={{ base: 'transparent', md: 'rgba(255, 255, 255, 0.05)' }}
          backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
        >
          <VStack alignItems='center'>
            <Text pt={1} color='gray.300' fontWeight={600}>
              Next Move
            </Text>
            <Text color='white' fontSize={'xl'} fontWeight={600}>
              Wait {(countdown / 3600000).toFixed(0)} hours
            </Text>
          </VStack>
        </Box>
      </HStack>
    </Box>
  )
}
