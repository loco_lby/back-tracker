import React, { useRef, useState, useEffect } from 'react'
import Map from '../components/Map'
import Layout from '../components/Layout'
import { supabase } from '../utils/supabaseClient'
import Notes from '../components/Notes'
import { useDisclosure } from '@chakra-ui/react'
import { useRouter } from 'next/router'

export default function Home() {
  const mapContainer = useRef(null)
  const map = useRef(null)
  const noteRef = useRef(null)

  const router = useRouter()

  const [lng, setLng] = useState()
  const [lat, setLat] = useState()
  const [zoom, setZoom] = useState()

  const [markerMode, setMarkerMode] = useState(false)
  const [marker, setMarker] = useState(null)
  const [cursor, setCursor] = useState('grab')
  const [countdown, setCountdown] = useState(0)

  useEffect(() => {
    // make a countdown timer
    if (countdown > 0) {
      setTimeout(() => {
        setCountdown(countdown - 1)
      }, 1000)
    }
  }, [countdown])

  const [items, setItems] = useState([])
  const [item, setItem] = useState(null)
  const [isDeleteLoading, setIsDeleteLoading] = useState(false)

  const { isOpen, onOpen, onClose } = useDisclosure()

  const user = supabase.auth.user()

  useEffect(() => {
    if (!user) {
      router.push('/login')
    }
  }, [])

  useEffect(() => {
    map.current.on('move', () => {
      setLng(map.current.getCenter().lng.toFixed(1))
      setLat(map.current.getCenter().lat.toFixed(1))
      setZoom(map.current.getZoom().toFixed(1))
    })
  }, [])

  useEffect(() => {
    if (user) {
      supabase
        .from('todos')
        .select('*')
        .eq('user_id', user?.id)
        .order('id', { ascending: false })
        .then(({ data, error }) => {
          if (!error) {
            setItems(data)
          }
        })
    }
  }, [user, items])

  useEffect(() => {
    const todoListener = supabase
      .from('todos')
      .on('*', (payload) => {
        if (payload.eventType !== 'DELETE') {
          const newTodo = payload.new
          setTodos((oldTodos) => {
            const exists = oldTodos.find((todo) => todo.id === newTodo.id)
            let newTodos
            if (exists) {
              const oldTodoIndex = oldTodos.findIndex(
                (obj) => obj.id === newTodo.id
              )
              oldTodos[oldTodoIndex] = newTodo
              newTodos = oldTodos
            } else {
              newTodos = [...oldTodos, newTodo]
            }
            newTodos.sort((a, b) => b.id - a.id)
            return newTodos
          })
        }
      })
      .subscribe()

    return () => {
      todoListener.unsubscribe()
    }
  }, [])

  const handleMarker = () => {
    onOpen()
  }

  const openHandler = (clickedTodo) => {
    setItem(clickedTodo)
  }

  const deleteHandler = async (todoId) => {
    setIsDeleteLoading(true)
    const { error } = await supabase.from('todos').delete().eq('id', todoId)
    if (!error) {
      setItems(items.filter((item) => item.id !== todoId))
    }
    setIsDeleteLoading(false)
  }

  useEffect(() => {
    if (markerMode === true) {
      setCursor('crosshair')

      map.current.on('click', (e) => {
        setMarker([e.lngLat.lng, e.lngLat.lat])
        setCursor('grab')
        setMarkerMode(false)
        setCountdown(86400000)
      })
    }
  }, [markerMode])

  return (
    <Layout
      lng={lng}
      lat={lat}
      zoom={zoom}
      handleMarker={handleMarker}
      noteRef={noteRef}
      countdown={countdown}
    >
      <Map
        mapContainer={mapContainer}
        map={map}
        lng={lng}
        lat={lat}
        zoom={zoom}
        cursor={cursor}
        marker={marker}
        markerMode={markerMode}
      />
      <Notes
        countdown={countdown}
        setMarkerMode={setMarkerMode}
        items={items}
        onClose={onClose}
        noteRef={noteRef}
        isOpen={isOpen}
        item={item}
        setItem={setItem}
        openHandler={openHandler}
        deleteHandler={deleteHandler}
        isDeleteLoading={isDeleteLoading}
      />
    </Layout>
  )
}
