import { ChakraProvider } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { supabase } from '../utils/supabaseClient';
function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const user = supabase.auth.user();

  useEffect(() => {
    const { data: authListener } = supabase.auth.onAuthStateChange(
      (event, session) => {
        handleAuthSession(event, session);
        if (event === 'SIGNED_IN') {
          const signedInUser = supabase.auth.user();
          const userId = signedInUser.id;
          supabase
            .from('profiles')
            .upsert({ id: userId })
            .then((_data, error) => {
              if (!error) {
                router.push('/');
              }
            });
        }
        if (event === 'SIGNED_OUT') {
          router.push('/login');
        }
      }
    );

    return () => {
      authListener.unsubscribe();
    };
  }, [router]);

  useEffect(() => {
    if (user) {
      if (router.pathname === '/login') {
        router.push('/');
      }
    }
  }, [router.pathname, user, router]);

  const handleAuthSession = async (event, session) => {
    await fetch('/api/auth', {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      credentials: 'same-origin',
      body: JSON.stringify({ event, session }),
    });
  };

  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default MyApp;
