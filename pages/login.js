import { Box } from '@chakra-ui/react';
import React, { useState, useEffect } from 'react';
import { supabase } from '../utils/supabaseClient';
import Auth from '../components/Auth';
import Account from '../components/Account';
export default function Login() {
  const [session, setSession] = useState(null);
  useEffect(() => {
    setSession(supabase.auth.session());

    supabase.auth.onAuthStateChange((_event, session) => {
      setSession(session);
    });
  }, []);
  return (
    <Box
      h='100vh'
      w='100%'
      overflow={'hidden'}
      bg='#405CB0'
      display={'flex'}
      justifyContent='center'
      alignItems={'center'}
      pos='relative'
    >
      <Box
        borderRadius={'10px'}
        bg={{ base: 'rgba(255, 255, 255, 0.05)' }}
        backdropFilter={{ base: 'blur(0)', md: 'blur(6.7px)' }}
        padding='30px'
        w='40%'
      >
        {!session ? (
          <Auth />
        ) : (
          <Account key={session.user.id} session={session} />
        )}
      </Box>
    </Box>
  );
}
